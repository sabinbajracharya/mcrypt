//
//  MCryptTests.swift
//  MCryptTests
//
//  Created by Sabin Bajracharya on 5/13/18.
//  Copyright © 2018 NepaliPatro. All rights reserved.
//

import XCTest
@testable import MCrypt

class MCryptTests: XCTestCase {
    
    var content: String!
    
//    let NPKEY_H = "x3b4zxx7kv531rhm"
//    let NPKEY_I = "e98dy0yxy3ua5p2k"
    
    let NPKEY_H = "5dcm1mwb986bd1lv"
    let NPKEY_I = "v08k2osndbwd9d7k"
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
//        content = "8bd35539f176b0d5f94386df9433cf50a363c3b9d10f6d901af003456e2d1ea7"
        content = "9ffb4369bf64358f791e96cabe50f4d63ead8deae8639553a5adff652e7f1fe9bf3d7aeea0e2f954af6fb9826d89a647b724438ce967c59afcfa4fe539cb5af2" //"\u{1}\u{2}\u{1}5b0b37a76498391ce5a04c173656dcb∏" //getEncryptedString()
//        content = getEncryptedString()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDecryption() {
        let decryptedContent = content.npDecrypt(withIv: NPKEY_H, andSk: NPKEY_I)
        
        assert(decryptedContent != nil)
        
        let list = decryptedContent!.split(separator: "+")
        var result = ""
        for item in list {
            let number = Int(item) ?? 0
            let unicode = Unicode.Scalar(number)
            
            if let _ = unicode {
                content = String(unicode!)
                result.append(content)
            }
        }
        
        print(result)
        assert(result == "बज्राचार्य")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
