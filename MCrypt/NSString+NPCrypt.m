//
//  NSString+NPCrypt.m
//  Nepali Patro
//
//  Created by Birijan Maharjan on 8/7/16.
//  Copyright © 2016 Nepali Patro. All rights reserved.
//


#import "NSString+NPCrypt.h"
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonKeyDerivation.h>

#import "NSData+NPCrypt.h"

@implementation NSString (NPCrypt)

const CCAlgorithm kAlgorithm = kCCAlgorithmAES128;
const NSUInteger kAlgorithmBlockSize = kCCBlockSizeAES128;

- (NSData *) hexStringToData {
    const char *chars = [self UTF8String];
    int i = 0;
    NSUInteger len = self.length;
    
    NSMutableData *data = [NSMutableData dataWithCapacity:len / 2];
    char byteChars[3] = {'\0','\0','\0'};
    unsigned long wholeByte;
    
    while (i < len) {
        byteChars[0] = chars[i++];
        byteChars[1] = chars[i++];
        wholeByte = strtoul(byteChars, NULL, 16);
        [data appendBytes:&wholeByte length:1];
    }
    
    return data;
}

- (NSString *)reverseString {
    NSMutableString *reversedString = [NSMutableString string];
    NSInteger charIndex = [self length];
    while (charIndex > 0) {
        charIndex--;
        NSRange subStrRange = NSMakeRange(charIndex, 1);
        [reversedString appendString:[self substringWithRange:subStrRange]];
    }
    
    return reversedString;
}

- (NSString *) NPDecryptWithIv: (NSString *)ivstr AndSk: (NSString *)skstr {
    //NSString *ivstr = @"p3b8zxw7oy531rhe";
    //NSString *skstr = @"v96cy9yvy3ua7n7k";
    size_t outLength;
    
    NSData *ivdata = [ivstr dataUsingEncoding:NSUTF8StringEncoding];
    NSData *skdata = [skstr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *revString = self;
    NSData *cipherData = [revString hexStringToData];
    NSMutableData *plainData = [NSMutableData dataWithLength:cipherData.length +
                                kAlgorithmBlockSize];
    CCCryptorStatus result = CCCrypt(kCCDecrypt, // operation
                                     kAlgorithm, // Algorithm
                                     kCCOptionPKCS7Padding, // options
                                     [skdata bytes], // key
                                     [skdata length], // keylength
                                     [ivdata bytes],// iv
                                     cipherData.bytes, // dataIn
                                     cipherData.length, // dataInLength,
                                     plainData.mutableBytes, // dataOut
                                     plainData.length, // dataOutAvailable
                                     &outLength);
    
    NSData *decodedData  = [[NSData alloc] init];
    if (result == kCCSuccess) {
        plainData.length = plainData.length - [plainData numberOfTrailingZeroBytes];
        NSString *decodedBase64 = [[NSString alloc] initWithData:plainData encoding:NSUTF8StringEncoding];
        return decodedBase64;
        @try {
            decodedData = [[NSData alloc] initWithBase64EncodedString:decodedBase64 options:0];
        } @catch(NSException *exception)  {
            //return [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
        }
        //return [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
    }
    return [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
}

@end
