//
//  NSData+NPCrypt.m
//  Nepali Patro
//
//  Created by Birijan Maharjan on 8/7/16.
//  Copyright © 2016 Nepali Patro. All rights reserved.
//

#import "NSData+NPCrypt.h"

@implementation NSData (NPCrypt)

- (NSUInteger) numberOfTrailingZeroBytes {
    int trim = 0;
    Byte zero = 0;
    NSData *zeroData = [NSData dataWithBytes: &zero length: 1];
    
    for ( NSInteger i = self.length -1 ; i >= 0; i--) {
        if ([[self subdataWithRange:NSMakeRange(i, 1)] isEqualToData:zeroData])
            trim++;
        else
            break;
    }
    
    return trim;
}

@end
