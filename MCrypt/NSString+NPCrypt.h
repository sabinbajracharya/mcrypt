//
//  NSString+NPCrypt.h
//  Nepali Patro
//
//  Created by Birijan Maharjan on 8/7/16.
//  Copyright © 2016 Nepali Patro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NPCrypt)

- (NSString *) NPDecryptWithIv: (NSString *)ivstr AndSk: (NSString *)skstr;

@end
