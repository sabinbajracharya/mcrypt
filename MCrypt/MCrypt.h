//
//  MCrypt.h
//  MCrypt
//
//  Created by Sabin Bajracharya on 5/13/18.
//  Copyright © 2018 NepaliPatro. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MCrypt.
FOUNDATION_EXPORT double MCryptVersionNumber;

//! Project version string for MCrypt.
FOUNDATION_EXPORT const unsigned char MCryptVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MCrypt/PublicHeader.h>


#import <MCrypt/NSString+NPCrypt.h>
#import <MCrypt/NSData+NPCrypt.h>
